package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"encoding/json"

	"github.com/PuerkitoBio/goquery"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

// Book Struct Model
type Book struct {
	Isbn   string  `json:"isbn"`
	Title  string  `json:"title"`
	Author string  `json:"author"`
	Price  float32 `json:"price"`
}

var db *sql.DB

func init() {
	var err error
	db, err = sql.Open("postgres", "postgres://bond:bond@localhost/bookstore?sslmode=disable")
	if err != nil {
		panic(err)
	}

	if err = db.Ping(); err != nil {
		panic(err)
	}
	fmt.Println("You connected to your database.")
}

func booksIndex(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, http.StatusText(405), http.StatusMethodNotAllowed)
		return
	}

	rows, err := db.Query("SELECT * FROM books")
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	defer rows.Close()

	bks := make([]Book, 0)
	for rows.Next() {
		bk := Book{}
		err := rows.Scan(&bk.Isbn, &bk.Title, &bk.Author, &bk.Price) // order matters
		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}
		bks = append(bks, bk)
	}
	if err = rows.Err(); err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}

	// for _, bk := range bks {
	// 	fmt.Fprintf(w, "%s, %s, %s, $%.2f\n", bk.Isbn, bk.Title, bk.Author, bk.Price)
	// }

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(bks)
}

func booksShow(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, http.StatusText(405), http.StatusMethodNotAllowed)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	Isbn := params["isbn"]

	if Isbn == "" {
		http.Error(w, http.StatusText(400), http.StatusBadRequest)
		return
	}

	row := db.QueryRow("SELECT * FROM books WHERE isbn = $1", Isbn)
	fmt.Println(row)

	bk := Book{}
	err := row.Scan(&bk.Isbn, &bk.Title, &bk.Author, &bk.Price)
	switch {
	case err == sql.ErrNoRows:
		json.NewEncoder(w).Encode(&Book{})
		return
	case err != nil:
		http.Error(w, http.StatusText(500), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(bk)
}

func booksCreateProcess(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, http.StatusText(405), http.StatusMethodNotAllowed)
		return
	}
	w.Header().Set("Content-Type", "application/json")

	// get form values
	bk := Book{}
	_ = json.NewDecoder(r.Body).Decode(&bk)

	// validate form values
	if bk.Isbn == "" || bk.Title == "" || bk.Author == "" {
		http.Error(w, http.StatusText(400), http.StatusBadRequest)
		return
	}

	fmt.Println(bk)

	// convert form values
	// f64, err := strconv.ParseFloat(p, 32)
	// if err != nil {
	// 	http.Error(w, http.StatusText(406)+"Please hit back and enter a number for the Price", http.StatusNotAcceptable)
	// 	return
	// }

	// insert values
	_, err := db.Exec("INSERT INTO books (isbn, title, author, price) VALUES ($1, $2, $3, $4)", bk.Isbn, bk.Title, bk.Author, bk.Price)
	if err != nil {
		fmt.Println(err)
		http.Error(w, http.StatusText(500), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(bk)

	// confirm insertion
	// tpl.ExecuteTemplate(w, "created.gohtml", bk)
}

func booksUpdateProcess(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, http.StatusText(405), http.StatusMethodNotAllowed)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	//params := mux.Vars(r)

	// get form values
	bk := Book{}
	_ = json.NewDecoder(r.Body).Decode(&bk)

	// validate form values
	if bk.Isbn == "" || bk.Title == "" || bk.Author == "" {
		http.Error(w, http.StatusText(400), http.StatusBadRequest)
		return
	}

	// insert values
	_, err := db.Exec("UPDATE books SET Isbn = $1, Title=$2, Author=$3, Price=$4 WHERE Isbn=$1;", bk.Isbn, bk.Title, bk.Author, bk.Price)
	if err != nil {
		http.Error(w, http.StatusText(500), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(bk)
	// confirm insertion
	// tpl.ExecuteTemplate(w, "updated.gohtml", bk)
}

func booksDeleteProcess(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	Isbn := params["isbn"]

	if Isbn == "" {
		http.Error(w, http.StatusText(400), http.StatusBadRequest)
		return
	}

	// delete book
	_, err := db.Exec("DELETE FROM books WHERE Isbn=$1;", Isbn)
	if err != nil {
		http.Error(w, http.StatusText(500), http.StatusInternalServerError)
		return
	}
	//http.Redirect(w, r, "/books", http.StatusSeeOther)
}

func scrapeHTML(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	resp, err := http.Get("https://www.bookdepository.com/textbooks")
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", resp.StatusCode, resp.Status)
	}
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	bks := make([]Book, 0)
	doc.Find(".book-item").Each(func(i int, s *goquery.Selection) {
		content, hasAttr := s.Find("meta").First().Attr("content")
		var isbn string
		var priceFloat float32
		if hasAttr {
			isbn = content
		}
		title := strings.TrimSpace(s.Find(".title").Find("a").Text())
		author := strings.TrimSpace(s.Find(".author").Find("a").Text())
		price := s.Find(".price").Text()

		rawPrice := strings.TrimSpace(strings.NewReplacer("€", "",
			",", ".").Replace(price))
		value, err := strconv.ParseFloat(rawPrice, 32)
		if err != nil {
			priceFloat = 0.0
		} else {
			priceFloat = float32(value)
		}
		bks = append(bks, Book{Isbn: isbn, Title: title, Author: author, Price: priceFloat})
	})

	saveScrapedBooksToDb := func(books []Book) error {
		valueStrings := make([]string, 0, len(books))
		valueArgs := make([]interface{}, 0, len(books)*4)
		for index, book := range books {
			valueStrings = append(valueStrings, "($"+strconv.Itoa(1+index*4)+",$"+strconv.Itoa(2+index*4)+",$"+strconv.Itoa(3+index*4)+",$"+strconv.Itoa(4+index*4)+")")
			valueArgs = append(valueArgs, book.Isbn)
			valueArgs = append(valueArgs, book.Title)
			valueArgs = append(valueArgs, book.Author)
			valueArgs = append(valueArgs, book.Price)
		}
		stmt := fmt.Sprintf("INSERT INTO books (isbn, title, author, price) VALUES %s", strings.Join(valueStrings, ","))
		_, err := db.Exec(stmt, valueArgs...)
		return err
	}
	dbErr := saveScrapedBooksToDb(bks)
	if dbErr != nil {
		fmt.Println(dbErr)
		http.Error(w, http.StatusText(400), http.StatusBadRequest)
		return
	} else {
		json.NewEncoder(w).Encode(bks)
	}

	//io.Copy(os.Stdout, resp.Body)
	//w.Header().Set("Content-Type", "application/json")
	//json.NewEncoder(w).Encode(resp.Body)
}

func main() {

	r := mux.NewRouter()

	r.HandleFunc("/books", booksIndex).Methods("GET")
	r.HandleFunc("/books/{isbn}", booksShow).Methods("GET")
	r.HandleFunc("/books/create", booksCreateProcess).Methods("POST")
	r.HandleFunc("/books/update/{isbn}", booksUpdateProcess).Methods("PUT")
	r.HandleFunc("/books/delete/{isbn}", booksDeleteProcess).Methods("DELETE")
	r.HandleFunc("/scrape", scrapeHTML).Methods("GET")
	http.ListenAndServe(":8000", r)
}
